import os
import cv2
import classificator as cl
import fillword_letters as fl
import fillword_image as fi

images = ('7x7.png', '5x5.png', '4x4.png', '3x3.png',
          '4x4-2.png', '4x4-3.png', '4x4-4.png', '4x4-5.png', '4x4-6.png', '6x6.png')
image_files = list(map(lambda name: os.path.join(
    os.getcwd(), 'test', name), images))


def read_letters(files):
    letters = []
    for file in files:
        img = cv2.imread(file)
        cnt = fi.count_squares(img)
        masked = fl.mask(img)
        images = fi.split_squares(masked, cnt)
        for image in images:
            letter = fl.crop_letter(image)
            letters.append(letter)
    return letters


def train(files):
    letters = read_letters(files)
    # cv2.imshow('letters', letters[1])
    cl.cl.make_dirs('АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ')
    for letter in letters:
        cl.cl.save_letter(letter)


train(image_files)
