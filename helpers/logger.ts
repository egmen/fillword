import winston, { createLogger, format, transports } from "winston";

const { combine, timestamp, printf, colorize } = format;

// Логи в консоль включены всегда
const myFormat = printf(
  (info) => `${info.timestamp} - ${info.level}: ${info.message}`
);

const formats = [timestamp(), myFormat, colorize()];

const logger = createLogger({
  level: "info",
  format: false
    ? format.json({
        replacer(key, value) {
          if (key === "level") {
            return winston.config.syslog.levels[value as string] ?? 6;
          }
          return value;
        },
      })
    : combine(...formats),
  transports: [new transports.Console()],
});

export default logger;
