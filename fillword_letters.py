import cv2
import math
import numpy as np
import fillword_image as fi


def mask(img):
    # letterColor = (63, 47, 37)
    copy = img.copy()
    lower = np.uint8([51, 42, 34])
    higher = np.uint8([77, 54, 50])
    mask = cv2.inRange(copy, lower, higher)
    return cv2.bitwise_not(mask)


def dilate(img):
    dilation_size = 2
    elm = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                    (2*dilation_size + 1,
                                     2*dilation_size+1),
                                    (dilation_size, dilation_size))
    mask = cv2.dilate(img, elm)
    return mask


def boundingCountours(contours):
    x = math.inf
    y = math.inf
    x1 = 0
    y1 = 0
    for contour in contours:
        for coord in contour:
            if x > coord[0][1]:
                x = coord[0][1]
            if x1 < coord[0][1]:
                x1 = coord[0][1]
            if y > coord[0][0]:
                y = coord[0][0]
            if y1 < coord[0][0]:
                y1 = coord[0][0]
    return x, y, x1, y1


def crop_letter(img):
    thresh = cv2.threshold(img, 250, 255, cv2.THRESH_BINARY_INV)
    contours, _ = cv2.findContours(
        dilate(thresh[1]), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    x, y, x1, y1 = boundingCountours(contours)
    res = img[x:x1, y:y1]
    return res


def backgroung_color(img):
    colors_count = {}
    (channel_b, channel_g, channel_r) = cv2.split(img)
    channel_b = channel_b.flatten()
    channel_g = channel_g.flatten()
    channel_r = channel_r.flatten()

    for i in range(len(channel_b)):
        RGB = (channel_r[i], channel_g[i], channel_b[i])
        if RGB in colors_count:
            colors_count[RGB] += 1
        else:
            colors_count[RGB] = 1
    max_key = ''
    max_count = 0
    for keys in sorted(colors_count, key=colors_count.__getitem__):
        if colors_count[keys] > max_count:
            max_count = colors_count[keys]
            max_key = keys
    return max_key


def is_used(img):
    color = backgroung_color(img)
    (b, g, r) = color
    if b > 210 and g > 210 and r > 210:
        return False
    return True


def parse_used(img, cnt):
    letters = fi.split_squares(img, cnt)
    used = map(is_used, letters)
    return list(used)
