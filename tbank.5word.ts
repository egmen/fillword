import {
  ALL_LETTERS,
  calcAbsentLetters,
  calcExistsLetters,
  totalWordPercent,
  WORD_5_VOCABULARY,
} from "./tbank.helpers";

// Настройки здесь
/** Точное положение присутствующих букв */
const MASK = ["", "", "", "", ""];
/** Другие присутствующие буквы */
const EXISTS_LETTERS = "";
/** Ошибочные слова */
const NEGATIVE_MASK_LIST = ["", "", "", "", ""];
// Конец настроек

const existsLetters = calcExistsLetters(EXISTS_LETTERS, MASK);
const aviableLetters = ALL_LETTERS.filter(
  (letter) =>
    !calcAbsentLetters(NEGATIVE_MASK_LIST, existsLetters.join("")).includes(
      letter
    )
).join("");

const regPattern = MASK.map((letter, letterIdx) => {
  if (letter !== "") {
    return letter;
  }
  let clearedLetters = aviableLetters;
  // Освобождаем места от букв, которых точно нет в выбранном месте
  NEGATIVE_MASK_LIST.forEach((negativeWord) => {
    const negativeLetter = negativeWord[letterIdx];
    clearedLetters = clearedLetters.replace(negativeLetter, "");
  });
  return `[${clearedLetters}]`;
}).join("");

const regMask = new RegExp(`^${regPattern}$`);

console.log("Регулярка", regMask);

const rightWords = WORD_5_VOCABULARY.filter((word) => regMask.test(word))
  // Слово содержит каждую из присутствующих букв
  .filter((word) =>
    existsLetters.every((existLetter) => word.includes(existLetter))
  )
  .map((word) => ({ word, percent: totalWordPercent(word) }))
  .sort((word1, word2) => word1.percent - word2.percent);
console.log(`Подходящие слова:`);
console.table(rightWords);
