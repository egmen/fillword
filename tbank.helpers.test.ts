import { describe, expect, it } from "vitest";
import { calcAbsentLetters, calcExistsLetters } from "./tbank.helpers";

describe("Модуль tinkoff.helper", () => {
  it("должен корректно вычислять отсутсвующие буквы", () => {
    const word = ["солка", "привет"];
    const existsLetters = "сае";

    const absentLetters = calcAbsentLetters(word, existsLetters);

    expect(absentLetters).toBe("олкпривт");
  });

  it("должен корректно вычислять присутствующие буквы", () => {
    const MASK = ["", "а"];
    const EXISTS_LETTERS = "се";

    const existsLetters = calcExistsLetters(EXISTS_LETTERS, MASK);

    expect(existsLetters).toStrictEqual(["с", "е", "а"]);
  });
});
