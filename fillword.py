import os
import math
import numpy as np
import fillword_image as fi
import fillword_letters as fl
import classificator as cl

vocabulary_file = 'vocabulary.txt'
with open(vocabulary_file, encoding='utf-8') as f:
    vocabulary = f.read().splitlines()
vocabulary_starters = set(map(lambda word: word[:3], vocabulary))

images_path = os.path.join(os.getcwd(), 'images')
moon_image = os.path.join(images_path, 'moon.png')
sun_image = os.path.join(images_path, 'sun.png')
close_image = os.path.join(images_path, 'close.png')


def parse_fillword(img):
    cnt = fi.count_squares(img)
    size = (int)(math.sqrt(cnt))
    letter_mask = fl.mask(img)
    used = fl.parse_used(img, cnt)
    letters = fi.split_squares(letter_mask, cnt)
    fw = []
    for i in range(cnt):
        letter = fl.crop_letter(letters[i])
        sym = cl.cl.classify_letter(letter)
        fw.append((i % size, i // size, sym, used[i]))
    return fw


def is_sibling(l1, l2):
    X = 0
    Y = 1
    # Находится слева
    if l1[X] == l2[X] + 1 and l1[Y] == l2[Y]:
        return True
    # Находится справа
    if l1[X] == l2[X] - 1 and l1[Y] == l2[Y]:
        return True
    # Находится сверху
    if l1[X] == l2[X] and l1[Y] == l2[Y] + 1:
        return True
    # Находится снизу
    if l1[X] == l2[X] and l1[Y] == l2[Y] - 1:
        return True
    return False


def find_next_word(word, fw):
    rest_letters = list(filter(lambda letter: not (
        letter in word) and not letter[3], fw))
    np.random.shuffle(rest_letters)
    if not word:
        return rest_letters
    last_letter = word[len(word) - 1]
    sibling_letters = filter(lambda letter: is_sibling(
        last_letter, letter), rest_letters)
    return map(lambda letter: word + [letter], sibling_letters)


def get_next_words(words, fw):
    next_words = []
    for word in words:
        next_words += find_next_word(word, fw)
    return next_words


def find_all_small_words(fw):
    words = []
    for l1 in find_next_word([], fw):
        w1 = [l1]
        for w2 in find_next_word(w1, fw):
            for w3 in find_next_word(w2, fw):
                words.append(w3)
    return words


def is_in_vocabulary(word):
    word_string = ''.join(x[2] for x in word).lower()
    return word_string in vocabulary


def filter_by_vocabulary(words):
    return list(filter(is_in_vocabulary, words))


def is_in_start(word):
    word_start = ''.join(x[2] for x in word).lower()[:3]
    return word_start in vocabulary_starters


def filter_by_start(words):
    return list(filter(is_in_start, words))
