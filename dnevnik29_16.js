import fs from "fs";
const vocFile = fs.readFileSync("./vocabulary.txt", "utf-8");
const voc = vocFile.split("\n");

const DATA = [
  ["утроба".split(""), "10000".split("")],
  ["оракул".split(""), "110".split("")],
  ["гамбит".split(""), "0000".split("")],
  ["клавир".split(""), "000".split("")],
  ["опекун".split(""), "1".split("")],
];
let ANSWER = ["о", "", "", "", "", ""];

function clearWordExistSymbols(answer, data) {
  answer.forEach((sym, symIdx) => {
    if (sym) {
      data.forEach((item) => {
        const [word, sugg] = item;
        const wordSymIdx = word.indexOf(sym);
        if (wordSymIdx === symIdx) {
          word[wordSymIdx] = " ";
          sugg.shift();
        } else if (wordSymIdx >= 0) {
          word[wordSymIdx] = " ";
          sugg.pop();
        }
      });
    }
  });
  return data;
}

function clearAbsentSymbols(data) {
  const emptyExist = data.find(([, sugg]) => sugg.length === 0);
  if (emptyExist) {
    const [emptyWord] = emptyExist;
    emptyWord.forEach((emptySym) => {
      data.forEach(([word]) => {
        const wordIdx = word.indexOf(emptySym);
        if (wordIdx > -1) {
          word[wordIdx] = " ";
        }
      });
    });
  }
  return data;
}

function clearData(data) {
  return data.filter(([word]) => {
    const symCount = word.filter((sym) => sym !== " ").length;
    return symCount !== 0;
  });
}

function clearExistSymbols(data) {
  const fullExistsSymbols = data.find(([word, sugg]) => {
    const symCount = word.filter((sym) => sym !== " ").length;
    if (symCount === sugg.length) {
      return true;
    }
  });
  if (fullExistsSymbols) {
    const [word] = fullExistsSymbols;
    const clearedWord = word.filter((sym) => sym !== " ");
    data.forEach(([wordForEmpty]) => {
      wordForEmpty.forEach((sym, symIdx) => {
        if (!clearedWord.includes(sym)) {
          wordForEmpty[symIdx] = " ";
        }
      });
    });
  }
  return data;
}

let clearedData = clearWordExistSymbols(ANSWER, DATA);
console.log(clearedData);
clearedData = clearAbsentSymbols(clearedData);
clearedData = clearData(clearedData);
console.log(clearedData, ANSWER);
// clearedData = clearExistSymbols(clearedData);

// console.log(clearedData);

const answers = voc.filter((word) => {
  if (/^о.....$/.test(word)) {
    const t = "[абвгилморт]";
    if (/^от[абвгилморт][абвгилморт][абвгилморт][абвгилморт]$/.test(word)) {
      return true;
    }
    if (/^о[абвгилморт]р[абвгилморт][абвгилморт][абвгилморт]$/.test(word)) {
      return true;
    }
    if (/^о[абвгилморт][абвгилморт][абвгилморт]б[абвгилморт]$/.test(word)) {
      return true;
    }
    if (/^о[абвгилморт][абвгилморт][абвгилморт][абвгилморт]а$/.test(word)) {
      return true;
    }
    if (/^о[абвгилморт][абвгилморт][абвгилморт]б[абвгилморт]$/.test(word)) {
      return true;
    }
  }
  return false;
});
console.log({ w: answers });
