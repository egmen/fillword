import fs from "fs";
import JSZip from "jszip";
import iconv from "iconv-lite";

// По требованию правообладателя доступ к разделу "Словари" на сайте http://speakrus.ru до выяснения обстоятельств временно прекращён. Приносим извинения. Некоторые материалы вы можете скачать отсюда
const VOCABULARY_LINK = "http://speakrus.ru/dict/zdf-win.zip";
const VOCABULARY_FILENAME = "zdf-win.txt";

async function unzip(file: Buffer, fileName: string): Promise<Buffer> {
  const zip = await JSZip.loadAsync(file);
  if (!zip) {
    throw new Error("Файл не является архивом");
  }
  const zippedFile = zip.file(fileName);
  if (!zippedFile) {
    throw new Error(`Файл ${fileName} не найден в архиве`);
  }
  return zippedFile.async("nodebuffer");
}

async function loadVocabulary(
  url: string,
  fileName: string
): Promise<string[]> {
  // const { data } = await axios(url, { responseType: "arraybuffer" });
  // fs.writeFileSync("data.zip", data);
  const data = fs.readFileSync("data.zip");
  const fileContent = await unzip(data, fileName);
  const decodedString = iconv.decode(fileContent, "win1251");
  const words = decodedString.split(/\r?\n/);

  return words.filter((word) => {
    if (word.length < 3) {
      return false;
    }
    if (word.includes("-")) {
      return false;
    }
    return true;
  });
}

async function install() {
  const vocabulary = await loadVocabulary(VOCABULARY_LINK, VOCABULARY_FILENAME);
  vocabulary.push("");
  fs.writeFileSync("vocabulary.txt", vocabulary.join("\n"));
  console.log(`Файл vocabulary.txt успешно записан ${vocabulary.length} слов`);
}

install();
