import os
import cv2
import numpy as np

fixed_letter_size = (100, 100)
letters_path = os.path.join(os.getcwd(), 'letters')

min_letter_samples = 2
max_letter_samples = 8


class Classificator():
    def __init__(self):
        self.hog = cv2.HOGDescriptor(
            _winSize=fixed_letter_size, _blockSize=(50, 50), _blockStride=(25, 25), _cellSize=(25, 25), _nbins=9, _gammaCorrection=True, _signedGradient=True)

        self.svm = cv2.ml.SVM_create()
        self.svm.setKernel(cv2.ml.SVM_RBF)
        self.svm.setC(12.5)
        self.svm.setGamma(0.50625)
        self.hsh = cv2.img_hash.BlockMeanHash_create()
        self.letters = {}
        self.letters_count = {}
        self.prepare_classifier()
        print('Classificator initiated')

    def computeHOGDescriptor(self, img):
        resized_image = cv2.resize(img, fixed_letter_size)
        return self.hog.compute(resized_image)

    def load_letters(self):
        if len(self.letters) > 0:
            return
        for root, subdirs, files in os.walk(letters_path):
            letter = os.path.basename(root)
            letters_count = len(files)
            if root == letters_path and letters_count > 0:
                raise Exception(
                    'Есть неклассифицированные изображения в {} - {} шт.'.format(letters_path, len(files)))
            if root != letters_path and letters_count <= min_letter_samples:
                if letters_count == 0:
                    message = 'Для буквы {} нет изображений'.format(letter)
                else:
                    message = 'Для буквы {} мало ({}) изображений'.format(
                        letter, letters_count)
                print(message)
            if root != letters_path and letters_count > max_letter_samples:
                message = 'Для буквы {} много ({}) изображений'.format(
                    letter, letters_count)
                print(message)
            for file in files:
                file_name = os.path.join(root, file)
                img = cv2.imdecode(np.fromfile(
                    file_name, np.uint8), cv2.IMREAD_UNCHANGED)
                hash_str = self.hash_image(img)
                self.letters[hash_str] = True

    def hash_image(self, img):
        hash_value = self.hsh.compute(img)
        hash_str = ''.join('{:02x}'.format(x) for x in hash_value[0])[:12]
        return hash_str

    def make_dirs(self, letters):
        for letter in letters:
            letter_path = os.path.join(letters_path, letter)
            os.makedirs(letter_path, exist_ok=True)

    def save_letter(self, img):
        self.load_letters()
        hash_str = self.hash_image(img)
        if hash_str in self.letters:
            return self.letters[hash_str]
        letter = self.classify_letter(img)
        # Защита от превышения количества букв
        if letter in self.letters_count and self.letters_count[letter] >= max_letter_samples:
            return False
        img_name = os.path.join(letters_path, '{}.png'.format(hash_str))
        cv2.imwrite(img_name, img)

    def prepare_classifier(self):
        samples = []
        labels = []
        for root, subdirs, files in os.walk(letters_path):
            if root == letters_path:
                continue
            letter = os.path.basename(root)
            self.letters_count[letter] = len(files)
            for file in files:
                file_name = os.path.join(root, file)
                img = cv2.imdecode(np.fromfile(
                    file_name, np.uint8), cv2.IMREAD_UNCHANGED)
                sample = self.computeHOGDescriptor(img)
                samples.append(sample)
                label_id = ord(letter)
                labels.append(label_id)
        samples = np.float32(samples)
        labels = np.array(labels)
        self.svm.train(samples, cv2.ml.ROW_SAMPLE, labels)

    def classify_letter(self, img):
        sample = self.computeHOGDescriptor(img)
        samples = np.float32([sample])
        label = self.svm.predict(samples)
        return chr(label[1][0][0])


cl = Classificator()
