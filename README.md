# Сборка OpenCV на Windows
- Собрать OPENCV к примеру в c:/opencv
- Запустить `pnpm do-install` в режиме администратора
- Запустить `pnpm install @u4/opencv4nodejs` в режиме администратора

- В переменных окружения должно быть OPENCV_BUILD_ROOT=C:\opencv
- Может быть OPENCV4NODEJS_DISABLE_AUTOBUILD=1
