import cv2
import math
import numpy as np


def mask(img):
    # fillwordColor = (86, 65, 67)
    copy = img.copy()
    lower = np.uint8([80, 60, 64])
    higher = np.uint8([89, 66, 69])
    mask = cv2.inRange(copy, lower, higher)
    return mask


def dilate(img):
    dilation_size = 2
    elm = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                    (2*dilation_size + 1,
                                     2*dilation_size+1),
                                    (dilation_size, dilation_size))
    mask = cv2.dilate(img, elm)
    return mask


def dirty_count(img):
    dilated = dilate(img)
    ret, thresh = cv2.threshold(dilated, 127, 255, 1)
    im2, contours = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return len(contours[0])


def count_squares(img):
    dirty_cnt = dirty_count(mask(img))
    clear_sqrt = round(math.sqrt(dirty_cnt), 0)
    if dirty_cnt == clear_sqrt ** 2:
        return dirty_cnt
    if dirty_cnt == 10:
        return 9
    if dirty_cnt >= 14 and dirty_cnt <= 18:
        return 16
    if dirty_cnt >= 20 and dirty_cnt <= 27:
        return 25
    message = 'Необработанное количество ячеек в филворде: {0:d}'.format(
        dirty_cnt)
    print(message)
    raise Exception(message)


def split_squares(img, cnt):
    sizeX = img.shape[1]
    sizeY = img.shape[0]
    nRows = (int)(math.sqrt(cnt))
    mCols = (int)(math.sqrt(cnt))
    images = []
    for i in range(0, nRows):
        for j in range(0, mCols):
            x = math.ceil(i*sizeY/nRows)
            y = math.ceil(i*sizeY/nRows + sizeY / nRows)
            x1 = math.ceil(j*sizeX/mCols)
            y1 = math.ceil(j*sizeX/mCols + sizeX / mCols)
            roi = img[x:y, x1:y1]
            images.append(roi)
    return images
