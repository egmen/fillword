import os
import numpy as np
import cv2 as cv2
# import fillword_image as fi
# import fillword_letters as fl
import fillword as f

# img = cv2.imread(os.path.join(os.getcwd(), 'test', '7x7.png'))
# img = cv2.imread(os.path.join(os.getcwd(), 'test', '5x5.png'))
# img = cv2.imread(os.path.join(os.getcwd(), 'test', '4x4.png'))
img = cv2.imread(os.path.join(os.getcwd(), 'test', '3x3.png'))

fw = f.parse_fillword(img)
words = f.find_all_small_words(fw)
# print(f.filter_by_vocabulary(words))
# print(f.filter_by_start(words))
print(fw)

cv2.imshow('fw', img)
cv2.waitKey()
