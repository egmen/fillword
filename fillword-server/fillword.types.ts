import { Rect } from "@u4/opencv4nodejs";

export type FillwordMeta = {
  xCenters: number[];
  yCenters: number[];
  cellSize: number;
};

export type ProtoCell = {
  /** Контур буквы */
  letterContour: Rect;
  /** Буква */
  letter: string;
};
