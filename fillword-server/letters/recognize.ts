import cv, { Mat } from "@u4/opencv4nodejs";
import path from "path";
import fs from "fs";
import { random } from "lodash";
import logger from "../../helpers/logger";

type LetterTemplate = {
  letter: string;
  image: Mat;
};

const ALL_LETTERS = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ".split("");

let isTemplatesLoaded = false;
const dirForTemplates = path.join(__dirname, "images");
const templateList: LetterTemplate[] = [];
const noTemplates = new Set<string>(ALL_LETTERS);

let isModelLoaded = false;
const dirForLetters = path.join(__dirname, "..", "..", "letters");
const svmFilename = path.join(__dirname, "lcletters.xml");
const svm = new cv.SVM({
  kernelType: cv.ml.SVM.RBF,
  c: 12.5,
  gamma: 0.50625,
});

export function recognizeLetter(image: Mat) {
  loadModel();
  const desc = computeHOGDescriptorFromImage(image);
  if (!desc) {
    throw new Error(`Computing HOG descriptor failed for image`);
  }
  const predictedLabel = svm.predict(desc);

  return String.fromCharCode(predictedLabel).replace("Щ", "Ш");
}

export function recognizeLetterByTemplate(image: Mat, originalLetter: Mat) {
  loadTemplates();

  const letterImage = prepareForLetterTemplate(image);
  const recognizedLetters = templateList
    .map((template) => {
      // cv.imwrite("1.png", letterImage);
      const result = letterImage.matchTemplate(
        template.image,
        cv.TM_CCOEFF_NORMED
      );
      const { maxVal } = result.minMaxLoc();
      return {
        maxVal,
        letter: template.letter,
      };
    })
    .sort((l1, l2) => l2.maxVal - l1.maxVal);

  const recognizedLetter = recognizedLetters.at(0);
  const randomFilename = path.join(dirForTemplates, `${random(100, 999)}.png`);

  // cv.imwrite(randomFilename, originalLetter);
  if (recognizedLetter && recognizedLetter.maxVal >= 0.8) {
    return recognizedLetter.letter;
  }

  const recognizeByModel = recognizeLetter(image);
  if (noTemplates.has(recognizeByModel)) {
    cv.imwrite(randomFilename, originalLetter);
  }
  return recognizeByModel;
}

/** Бинаризация изображения букв */
export function toBinaryImage(image: Mat) {
  // Преобразование в оттенки серого
  const grayImage = image.bgrToGray();

  // Применение бинаризации (пороговое значение можно подбирать)
  const binaryImage = grayImage.threshold(50, 255, cv.THRESH_BINARY_INV);

  return binaryImage;
}

/** Бинаризация изображения букв цветных */
export function toBinaryImageColorLetter(image: Mat) {
  // Преобразование в оттенки серого
  const grayImage = image.bgrToGray();

  // Применение бинаризации (пороговое значение можно подбирать)
  const binaryImage = grayImage.threshold(150, 255, cv.THRESH_BINARY_INV);

  return binaryImage;
}

export function getPositiveBinary(image: Mat) {
  return image.threshold(150, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU);
}

export function prepareForLetterTemplate(invertedImage: Mat) {
  const resizedImage = invertedImage.resize(40, 40);

  return resizedImage;
}

function loadModel() {
  if (isModelLoaded) {
    return;
  }
  svm.load(svmFilename);
  isModelLoaded = true;
}

function loadTemplates() {
  if (isTemplatesLoaded) {
    return;
  }

  fs.readdirSync(dirForTemplates).forEach((filename) => {
    const parsedFilename = path.parse(filename);

    const letter = parsedFilename.name;
    const isLetter = letter.length === 1;
    if (isLetter) {
      const letterFilename = path.join(dirForTemplates, filename);
      const letterFileContent = fs.readFileSync(letterFilename);
      const img = cv.imdecode(letterFileContent);

      const templateImage = prepareForLetterTemplate(img);
      const binaryImage = toBinaryImage(templateImage);
      const positiveBinaryImage = getPositiveBinary(binaryImage);

      noTemplates.delete(letter);
      templateList.push({
        letter,
        image: positiveBinaryImage,
      });
    }
  });
  if (noTemplates.size > 0) {
    logger.warn(`Не хватает шаблонов для: ${Array.from(noTemplates)}`);
  }
  isTemplatesLoaded = true;
}

function loadFiles() {
  return fs.readdirSync(dirForLetters).map((letter) => ({
    letter,
    files: fs.readdirSync(path.join(dirForLetters, letter)),
  }));
}

export function train() {
  const samples: number[][] = [];
  const labels: number[] = [];
  const letters = loadFiles();
  letters.forEach(({ letter, files }) => {
    files.forEach((filename) => {
      const fullLetterFilename = path.join(dirForLetters, letter, filename);
      const fileContent = fs.readFileSync(fullLetterFilename);
      const img = cv.imdecode(fileContent);
      const desc = computeHOGDescriptorFromImage(img);
      if (!desc) {
        console.warn(`Не удалось посчитать desc для ${fullLetterFilename}`);
        return;
      }

      samples.push(desc);
      labels.push(letter.charCodeAt(0));
    });
  });

  const trainData = new cv.TrainData(
    new cv.Mat(samples, cv.CV_32F),
    cv.ml.ROW_SAMPLE,
    new cv.Mat([labels], cv.CV_32S)
  );
  svm.train(trainData);
  svm.save(svmFilename);
}

const hog = new cv.HOGDescriptor({
  winSize: new cv.Size(40, 40),
  blockSize: new cv.Size(20, 20),
  blockStride: new cv.Size(10, 10),
  cellSize: new cv.Size(10, 10),
  L2HysThreshold: 0.2,
  nbins: 9,
  gammaCorrection: true,
  signedGradient: true,
});

const computeHOGDescriptorFromImage = (img: Mat) => {
  let im: Mat | null = img;
  if (im.rows !== 40 || im.cols !== 40) {
    im = im.resize(40, 40);
  }

  if (!im) {
    return null;
  }

  return hog.compute(im);
};
