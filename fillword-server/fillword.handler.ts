import fs from "node:fs";
import path from "node:path";
import logger from "../helpers/logger";
import { drawSolution, isButton, isScreen } from "./fillword.image";
import { sendNotify } from "./notify";
import { Point2 } from "@u4/opencv4nodejs";
import { Fillword } from "./fillword.class";
import { addBrutForceWord } from "./vocabulary";

// https://yandex.ru/games/app/100048

type StateScreenshot = {
  stage: "screenshot";
  image: string;
  rect: DOMRect;
};
type StateMove = {
  stage: "move";
};
type State = { stage: null } | StateScreenshot | StateMove;

const allSolutions = new Map<string, string[]>();

function ensureFile(filename: string) {
  try {
    fs.statSync(filename);
  } catch {
    fs.writeFileSync(filename, "");
  }
}

const scriptFilename = path.join(__dirname, "scripts", "fillword.move.js");
const moveScript = fs.readFileSync(scriptFilename, "utf-8");
const clearFillwordFilename = path.join(__dirname, "clear-fillword.png");
ensureFile(clearFillwordFilename);

let notifyTimeout = setTimeout(() => null);
function notifyNoSolve() {
  clearTimeout(notifyTimeout);
  notifyTimeout = setTimeout(() => {
    sendNotify("Не могу решить филлворд!");
    currentFillword = null;
    fs.rmSync(clearFillwordFilename);
  }, 1000 * 60 * 8);
}

function clickPoints(points: Point2[], rect: DOMRect) {
  const wayPoints = points.map((p) => [
    [Math.ceil(p.x + rect.x), Math.ceil(p.y + rect.y)],
  ]);
  return moveScript.replace(
    "const way = [];",
    `const way = ${JSON.stringify(wayPoints)};`
  );
}

let currentFillword: Fillword | null = null;

export function resetSolving() {
  if (currentFillword) {
    currentFillword.isSolved = false;
  }
}

export async function fillwordHandler({
  state,
}: {
  state: State;
}): Promise<string> {
  logger.info(`Текущая стадия: ${state.stage}`);
  try {
    if (!state.stage) {
      return "fillword.screenshot.js";
    }
    if (state.stage === "screenshot") {
      const { image, rect } = state;
      const [, img] = image.split(";base64,");

      const screenshot = Buffer.from(img, "base64");
      logger.info(`Получена картинка ${screenshot.length} байт`);
      if (screenshot.length < 10000) {
        logger.info(`Пустое изображении: ${screenshot.length} байт`);
        return "fillword.screenshot.js";
      }

      fs.writeFileSync("img.png", screenshot);

      const startScreenPoint = isButton(screenshot, "level");
      if (startScreenPoint) {
        logger.info("Стартовый экран");
        return clickPoints([startScreenPoint], rect);
      }
      const continueButtonPoint = isButton(screenshot, "continue");
      if (continueButtonPoint) {
        logger.info("Кнопка ПРОДОЛЖИТЬ");
        return clickPoints([continueButtonPoint], rect);
      }
      const getButtonPoint = isButton(screenshot, "get");
      if (getButtonPoint) {
        logger.info("Кнопка забрать монеты");
        return clickPoints([getButtonPoint], rect);
      }
      const refreshButton = isButton(screenshot, "refresh");
      if (refreshButton) {
        logger.info("Кнопка обновить игру");
        return clickPoints([refreshButton], rect);
      }

      const isSettingsScreen = isScreen(screenshot, "settings");
      if (isSettingsScreen) {
        logger.info("Экран настроек");
        const engButton = isButton(screenshot, "eng");
        const rusButton = isButton(screenshot, "rus");
        const closeButton = isButton(screenshot, "close");
        if (engButton && rusButton && closeButton) {
          logger.warn(`Сброс филлворда`);
          allSolutions.clear();
          return clickPoints([engButton, rusButton, closeButton], rect);
        }
      }

      if (!currentFillword) {
        try {
          const clearFillword = fs.readFileSync(clearFillwordFilename);
          if (clearFillword.byteLength === 0) {
            logger.error(`Пустой филлворд`);
            throw new Error(`Пустой филлворд`);
          }
          currentFillword = new Fillword(clearFillword);
        } catch {
          logger.error(`Не удалось загрузить чистый филлворд`);
        }
      }

      if (!currentFillword) {
        try {
          currentFillword = new Fillword(screenshot);
        } catch {
          logger.error(`Не удалось загрузить филлворд из скриншота`);
          throw new Error(`Не удалось загрузить филлворд из скриншота`);
        }
      }

      if (!currentFillword.isSameFillword(screenshot)) {
        try {
          const newFillword = new Fillword(screenshot);
          if (currentFillword.isSolvedBrutForce) {
            sendNotify(
              `Найдено слово: ${String(
                currentFillword.lastSolution?.word
              ).toUpperCase()} из ${currentFillword.solutions.length} вариантов`
            );
            addBrutForceWord(currentFillword.lastSolution?.word || "Ошибка");
          }
          currentFillword = newFillword;
          logger.info(`Новый филлворд!`);
        } catch (e) {
          logger.error(e);
          throw new Error(`Не удалось распознать филлворд`);
        }
      }

      if (currentFillword.isFullySolved) {
        logger.info(`Филлворд полностью решен!`);
        return "fillword.screenshot.js";
      }

      if (currentFillword.isFullyUnsolved) {
        notifyNoSolve();
        try {
          new Fillword(screenshot);
          fs.writeFileSync(clearFillwordFilename, screenshot);
        } catch (err) {
          logger.error(`Не обновляем файл филлворда`);
        }

        // const fixedClearFilename = `${cells
        //   .map((c) => c.letter)
        //   .join("")
        //   .slice(0, 8)}.png`;
        // fs.writeFileSync(fixedClearFilename, buf);
      }

      let sampleSolutions = currentFillword.getNextSolution();
      const sampleSolutionWords = sampleSolutions.map((s) => s.word);

      if (sampleSolutions.length === 0 && !currentFillword.isSolvedOnline) {
        await currentFillword.solveOnline();
        sampleSolutions = currentFillword.getNextSolution();
      }

      if (sampleSolutions.length === 0 && !currentFillword.isSolvedBrutForce) {
        currentFillword.solveBrutForce();
        logger.warn(`Режим брутфорса!`);
        sampleSolutions = currentFillword.getNextSolution();
      }

      logger.info(
        `Найдено ${currentFillword.sortedWords.length} решений (${
          currentFillword.usedSolutions.size
        } уже использованы): ${currentFillword.sortedWords
          .map((w) => (sampleSolutionWords.includes(w) ? `(${w})` : w))
          .join()}`
      );

      if (sampleSolutions.length === 0) {
        throw new Error(`Не удалось найти решения для филлворда`);
      }

      let imageWithSolution = screenshot;
      sampleSolutions.forEach((s) => {
        imageWithSolution = drawSolution(imageWithSolution, s.solution);
      });

      logger.info(
        `Выбраны слова (${sampleSolutions.length}): "${sampleSolutions
          .map((s) => s.word)
          .join(",")}"`
      );
      fs.writeFileSync("img.png", imageWithSolution);

      const attempts = sampleSolutions.map((s) => s.solution);

      const way = attempts.map((attempt) =>
        attempt.map(({ center }) => [center.x + rect.x, center.y + rect.y])
      );
      const wayObject = JSON.stringify(way);
      return moveScript.replace("const way = [];", `const way = ${wayObject};`);
    }
    if (state.stage === "move") {
      return "fillword.screenshot.js";
    }
  } catch (err) {
    logger.error(`Ошибка обработки: ${err}`);
    return "fillword.screenshot.js";
  }
  return "fillword.screenshot.js";
}
