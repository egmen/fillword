import { FillwordCell, UNSOLVED_CELL_COLOR } from "./fillword-cell.class";

/** Минимальное слово для решения = 5 букв */
export const MINIMUM_UNSOLVED_PART_LENGTH = 5;

export function getUnsolvedCells(cells: FillwordCell[]) {
  return cells.filter((c) => c.isUnsolved);
}

export function unsolvedCellCount(fillword: FillwordCell[]) {
  return getUnsolvedCells(fillword).length;
}

export function isUnsolved(fillword: FillwordCell[]) {
  return (
    fillword.length > 10 && unsolvedCellCount(fillword) === fillword.length
  );
}

/** Фильтрует из массива соседние ячейки */
export function filterSiblingCells(cell: FillwordCell, cells: FillwordCell[]) {
  return cells.filter((c) => isSiblingCell(cell, c));
}

/** Удаляет (мутирует) из исходного массива соседние ячейки */
export function extractSiblingCells(cell: FillwordCell, cells: FillwordCell[]) {
  const siblings: FillwordCell[] = [];
  cells.forEach((c, idx) => {
    const isSibling = isSiblingCell(cell, c);
    if (isSibling) {
      siblings.push(...cells.splice(idx, 1));
    }
  });

  return siblings;
}

export function isSiblingCell(c1: FillwordCell, c2: FillwordCell) {
  const diffX = Math.abs(c1.x! - c2.x!);
  const diffY = Math.abs(c1.y! - c2.y!);
  return (diffX === 1 && diffY === 0) || (diffX === 0 && diffY === 1);
}

export function generateUnsolvedFillword(word: string): FillwordCell[] {
  const letters = word.split("");
  const size = Math.sqrt(letters.length);
  const result: FillwordCell[] = [];

  for (let y = 1; y <= size; y++) {
    for (let x = 1; x <= size; x++) {
      const letter = letters.shift()!;
      // @ts-expect-error for test purposes
      const cell = new FillwordCell(x, y);
      cell.letter = letter;
      cell.color = UNSOLVED_CELL_COLOR;
      result.push(cell);
    }
  }

  return result;
}

export function getCell(
  cells: FillwordCell[],
  x: number,
  y: number
): FillwordCell | undefined {
  return cells.find((cell) => cell.x === x && cell.y === y);
}

/**
 * Получение всех нерешённых кусков филлворда
 *
 * нужно для предсказания полного решения филлворда (что слово не сделает филлвор нерешаемым)
 */
export function getUnsolvedParts(cells: FillwordCell[]) {
  const unsolvedParts: FillwordCell[][] = [];

  const unsolvedCells = getUnsolvedCells(cells);

  let unsolvedPart: FillwordCell[] | undefined = undefined;
  while (unsolvedCells.length > 0) {
    if (!unsolvedPart) {
      const cell = unsolvedCells.pop()!;
      unsolvedPart = [cell];
      unsolvedParts.push(unsolvedPart);
    } else {
      const siblings = unsolvedPart
        .map((cell) => extractSiblingCells(cell, unsolvedCells))
        .flat();
      if (siblings.length > 0) {
        unsolvedPart.push(...siblings);
      } else {
        unsolvedPart = undefined;
      }
    }
  }

  return unsolvedParts;
}

/** Признак возможности решения филлворда */
export function canBeSolved(cells: FillwordCell[]) {
  const unsolvedParts = getUnsolvedParts(cells);

  return (
    unsolvedParts.length > 0 &&
    unsolvedParts.every((part) => part.length >= MINIMUM_UNSOLVED_PART_LENGTH)
  );
}
