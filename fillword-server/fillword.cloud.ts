import * as cheerio from "cheerio";
import { appendWords } from "./vocabulary";

export const AnagramType = {
  /** Решить анаграмму */
  Solve: 1,
  /** Составить анаграмму */
  Create: 2,
  /** Составить слова из букв */
  Words: 3,
  /** Подобрать слова по маске */
  Mask: 4,
};

const BASE_URL = "https://anagram.poncy.ru/?inword=&answer_type=3";

export function parsePoncyResult(html: string) {
  const $ = cheerio.load(html);
  const results = $("a.resultquery");
  const words = results
    .map((_, elm) => {
      return $(elm).text();
    })
    .toArray();
  return words;
}

export async function resolveAnagram(
  letters: string,
  type: (typeof AnagramType)[keyof typeof AnagramType] = AnagramType.Words,
  nouns = true
): Promise<string[]> {
  const url = new URL(BASE_URL);
  url.searchParams.set("inword", letters);
  url.searchParams.set("answer_type", String(type));
  url.searchParams.set("nouns", String(type));
  const res = await fetch(url.href);

  const text = await res.text();

  const words = parsePoncyResult(text);
  appendWords(words);
  return words;
}
