import { describe, expect, it } from "vitest";
import fs from "fs";
import path from "path";
import {
  analyzeFillword,
  detectContours,
  calcColors,
  recognizeContours,
  filterBottomRows,
  calcFullFillword,
  calcFillwordMeta,
  compareFillword,
} from "./fillword.image";
import cv from "@u4/opencv4nodejs";
import {
  getPositiveBinary,
  toBinaryImage,
  toBinaryImageColorLetter,
} from "./letters/recognize";

function getImage(name: string): Buffer {
  const filename = `${name}.png`;
  const fullFilename = path.join(__dirname, "images", filename);

  return fs.readFileSync(fullFilename);
}

function getStringByTemplate(img: Buffer) {
  return analyzeFillword(img)
    .map((cell) => {
      return cell.letter;
    })
    .join("");
}

describe("Fillword Image Decoder", () => {
  it("should get image", () => {
    const img = getImage("fillword-1");

    expect(img).toHaveLength(114221);
  });

  it("should recognize letters-1", async () => {
    const img = getImage("fillword-1");

    const fillword = await analyzeFillword(img);

    expect(fillword.map((c) => c.letter).join("")).toBe(
      "ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ"
    );
    expect(fillword[0]).toMatchInlineSnapshot(`
      _FillwordCell {
        "cellContour": Rect {
          "height": 72,
          "width": 72,
          "x": 10,
          "y": 192,
        },
        "center": Point2 {
          "x": 46,
          "y": 228,
        },
        "color": "c275cc",
        "image": Mat {
          "channels": 3,
          "cols": 72,
          "depth": 0,
          "dims": 2,
          "elemSize": 3,
          "empty": 0,
          "rows": 72,
          "sizes": [
            72,
            72,
          ],
          "step": 1236,
          "type": 16,
        },
        "isStart": false,
        "letter": "О",
        "letterContour": Rect {
          "height": 51,
          "width": 46,
          "x": 22,
          "y": 203,
        },
        "x": 1,
        "y": 1,
      }
    `);
  });

  it("should recognize letters-2", async () => {
    const img = getImage("fillword-2");

    const fillword = await analyzeFillword(img);

    expect(fillword.map((c) => c.letter).join("")).toBe(
      "ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ"
    );
    expect(fillword[0]).toMatchInlineSnapshot(`
      _FillwordCell {
        "cellContour": Rect {
          "height": 70,
          "width": 70,
          "x": 10,
          "y": 126,
        },
        "center": Point2 {
          "x": 45,
          "y": 161,
        },
        "color": "c275cc",
        "image": Mat {
          "channels": 3,
          "cols": 70,
          "depth": 0,
          "dims": 2,
          "elemSize": 3,
          "empty": 0,
          "rows": 70,
          "sizes": [
            70,
            70,
          ],
          "step": 1200,
          "type": 16,
        },
        "isStart": false,
        "letter": "О",
        "letterContour": Rect {
          "height": 50,
          "width": 44,
          "x": 22,
          "y": 136,
        },
        "x": 1,
        "y": 1,
      }
    `);
  });

  it("should recognize letters-3", async () => {
    const img = getImage("fillword-3");

    const fillword = await analyzeFillword(img);

    expect(fillword.map((c) => c.letter).join("")).toBe(
      "УКФИЛХСЛОЬНЯОВМБРАГРУЗАШУ"
    );
  });

  it("should count color-1", async () => {
    const img = getImage("color-1");
    const image = cv.imdecode(img);

    const color = calcColors(image);

    expect(color).toEqual({
      isStart: false,
      color: "c275cc",
    });
  });

  it("should count color-2", async () => {
    const img = getImage("color-2");
    const image = cv.imdecode(img);

    const color = calcColors(image);

    expect(color).toEqual({
      isStart: true,
      color: "2f94ad",
    });
  });

  it("should count color-3", async () => {
    const img = getImage("color-3");
    const image = cv.imdecode(img);

    const color = calcColors(image);

    expect(color).toEqual({
      isStart: false,
      color: "dedee0",
    });
  });

  it("should recognize letters by template-1", () => {
    const img = getImage("fillword-1");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ");
  });

  it("should recognize letters by template-2", () => {
    const img = getImage("fillword-2");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("ОВЫВСДАКМУПОЖАКРРБОЕИНЦДТ");
  });

  it("should recognize letters by template-3", () => {
    const img = getImage("fillword-3");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("УКФИЛХСЛОЬНЯОВМБРАГРУЗАШУ");
  });

  it("should recognize letters by template-4", () => {
    const img = getImage("fillword-4");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("СОБАШЕЯИДТМЬЫНААВАПОЛСРЕК");
  });

  it("should recognize letters by template-5", () => {
    const img = getImage("fillword-5");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("ФЕЗВЕИРИГСКШУРЫАЧОПТАТЬНЕ");
  });

  it("should recognize letters by template-6", () => {
    const img = getImage("fillword-6");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("ЕЗУМАЙИРПТМЁШАХТИЕГРЫБДНА");
  });

  it("should recognize letters by template-7", () => {
    const img = getImage("fillword-7");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("КШИКЬТАТЯЖЕСЧПФЛОРЕЕВЕЦАМБОУЛИОДАНГН");
  });

  it("should recognize letters by template-8", () => {
    const img = getImage("fillword-8");

    const letters = getStringByTemplate(img);

    expect(letters).toContain("РЕДЙАВМАРКЕОЖОЛСРРНОСТЬПЦИВЕПБААКУЗА");
  });

  it("should recognize letters by template-9", () => {
    const img = getImage("fillword-9");

    const cells = analyzeFillword(img);

    expect(cells.map((c) => c.letter).join("")).toContain(
      "РЕДЙАВМАРКЕОЖОЛСРРНОСТЬПЦИВЕПБААКУЗА"
    );
  });

  it("should recognize letters by template-10 (unvisible T letter)", () => {
    const img = getImage("fillword-10");

    const cells = analyzeFillword(img);

    expect(cells.map((c) => c.letter).join("")).toContain(
      "МКЛАВИУДРОСШНИЕЬТАЕЧЕТПЗАКБОРАЕИВАЛГ"
    );
  });

  it("should recognize letters by template-11 (unvisible letters)", () => {
    const img = getImage("fillword-11");

    const cells = analyzeFillword(img);

    expect(cells.map((c) => c.letter).join("")).toContain(
      "НЁБЕРУОАГУЛСКПРОФИЫРТОЬЛВЗАСЕДОКЕИНА"
    );
  });

  it("should recognize letters by template-12", () => {
    const img = getImage("fillword-12");

    const cells = analyzeFillword(img);

    expect(cells.map((c) => c.letter).join("")).toContain(
      "ИЖТОВОМЦУАНЙБЕГОЛЬОРЗЕЪСИФДПРОЛИНТАК"
    );
  });

  it("should recognize letters by template-14", () => {
    const img = getImage("fillword-14");

    const cells = analyzeFillword(img);

    expect(cells.map((c) => c.letter).join("")).toContain(
      "ОЛЯСКРОКРОХАПСГОАКЙОЛРТАЛЕОПИСИМДРАБТИКАНИУТАНАВР"
    );
  });

  it.skip("should recognize letters by clear fillword template-15", () => {
    const img = getImage("fillword-15");
    const imgClear = getImage("fillword-15-clear");

    const clearCells = analyzeFillword(imgClear);
    const cells = compareFillword(img, imgClear);

    expect(clearCells.map((c) => c.letter).join("")).toContain(
      "ТСЕШСПАРЁРКПЕЧАЖИСАУХАКОТУЯАНЙЕФЕСТЬЕРАЗЦТУМСГЕБВАОТДАЛЕПРЫСКЕИН"
    );
    expect(cells.map((c) => c.letter).join("")).toContain(
      "ТСЕШСПАРЁРКПЕЧАЖИСАУХАКОТУЯАНЙЕФЕСТЬЕРАЗЦГЕБВОТДАЛЕПРЫСКЕИН"
    );
  });

  it.skip("should calculate metdata", () => {
    const img = getImage("fillword-11");
    const image = cv.imdecode(img);

    /** Основное изображение для вычисления метаданных */
    const invertedBinaryImage = toBinaryImage(image);
    const positiveMainImage = getPositiveBinary(invertedBinaryImage);

    /** Дополнительное изображение для нераспознанных символов */
    const invertedColoredImage = toBinaryImageColorLetter(image);

    /** Вычисление контуров */
    const contours = detectContours(invertedBinaryImage);
    /** Протоячейки */
    const protoCellsAll = recognizeContours(positiveMainImage, contours);
    /** Фильрация протоячеек */
    const protoCells = filterBottomRows(protoCellsAll);

    const meta = calcFillwordMeta(protoCells);

    expect(meta.yCenters).toEqual([155, 220, 285, 350, 415, 481]);
    expect(meta.xCenters).toEqual([38, 103, 168, 233, 298, 363]);
    expect(meta.cellSize).toEqual(65);
  });

  it.skip("should restore unvisible cells", () => {
    const img = getImage("fillword-11");
    const image = cv.imdecode(img);

    /** Основное изображение для вычисления метаданных */
    const invertedBinaryImage = toBinaryImage(image);
    const positiveMainImage = getPositiveBinary(invertedBinaryImage);

    /** Дополнительное изображение для нераспознанных символов */
    const invertedColoredImage = toBinaryImageColorLetter(image);

    /** Вычисление контуров */
    const contours = detectContours(invertedBinaryImage);
    /** Протоячейки */
    const protoCellsAll = recognizeContours(positiveMainImage, contours);
    /** Фильрация протоячеек */
    const protoCells = filterBottomRows(protoCellsAll);

    const colorLetters = calcFullFillword(image, protoCells);

    expect(colorLetters.length).toBe(36);
  });
});
