import { Mat, Point2, Rect } from "@u4/opencv4nodejs";
import { computed, observable } from "mobx";

export const UNSOLVED_CELL_COLOR = "dedee0";

export class FillwordCell {
  constructor(
    /** Порядковый номер столбца */
    public x: number,
    /** Порядковый номер ряда */
    public y: number,
    /** Цвет фона ячейки */
    public color: string,
    /** Картинка ячейки */
    public image: Mat,
    /** Габариты ячейки */
    public cellContour: Rect,
    /** Координаты центра */
    public center: Point2,
    /** Признак стартовой ячейки */
    private isStart: boolean
  ) {}
  /** Контур буквы */
  public letterContour?: Rect;
  /** Буква */
  @observable public letter = "";

  @computed get isUnsolved() {
    return this.color === UNSOLVED_CELL_COLOR;
  }

  @computed get isSolved() {
    return !this.isUnsolved;
  }

  @computed get letters() {
    if (/[ЕЁ]/.test(this.letter)) {
      return ["Е", "Ё"];
    }
    if (/[ШЩ]/.test(this.letter)) {
      return ["Ш", "Щ"];
    }
    if (/[ЬЪ]/.test(this.letter)) {
      return ["Ь", "Ъ"];
    }
    if (this.letter) {
      return [this.letter];
    }
    return [];
  }

  public clone() {
    return new FillwordCell(
      this.x,
      this.y,
      this.color,
      this.image,
      this.cellContour,
      this.center,
      this.isStart
    );
  }

  public setColor(color: string) {
    this.color = color;
    return this;
  }
}
